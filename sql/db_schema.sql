CREATE TABLE article (
    id bigint PRIMARY KEY,
    ebay character varying(255) NOT NULL,
    img character varying(255),
    title character varying(255),
    price money
);

CREATE TABLE article_article (
    id bigint PRIMARY KEY,
    cs_id bigint PRIMARY KEY
);

CREATE TABLE article_text (
    id bigint PRIMARY KEY,
    priority smallint PRIMARY KEY,
    content character varying(16384)
);

CREATE TABLE article_stammid (
    id bigint PRIMARY KEY,
    stammid bigint PRIMARY KEY
)