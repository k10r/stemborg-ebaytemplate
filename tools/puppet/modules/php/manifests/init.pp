class php {

  $phpVersion = 'present'

  package { "python-software-properties":
    ensure => 'present'
  }

  exec { "add php 5.4 repos":
    command => "/usr/bin/sudo /usr/bin/add-apt-repository ppa:ondrej/php5-oldstable && /usr/bin/sudo /usr/bin/apt-get update",
    require => Package["python-software-properties"]
  }


  package { "php5":
    ensure => $phpVersion,
    require => Exec["add php 5.4 repos"]
  }

  package { "php5-common":
    ensure => $phpVersion,
    require => Package["php5"]
  }


  package { "php5-cli":
    ensure => $phpVersion,
    require => Package["php5"]
  }


  package { "php5-intl":
    ensure => $phpVersion,
    require => Package["php5"]
  }

  package { "php-pear":
      ensure => $phpVersion,
    require => Package["php5-cli"]
  }

  package { "php5-memcache":
        ensure => $phpVersion,
      require => Package["php5"]
    }

  package { "php5-gd":
          ensure => $phpVersion,
        require => Package["php5"]
      }

  package { "php5-mysql":
    ensure => $phpVersion,
    require => Package["php-pear"]
  }

  exec { "php/composer":
    command => "/usr/bin/curl -sS https://getcomposer.org/installer > /tmp/composer-install.php && /usr/bin/php /tmp/composer-install.php && sudo mv composer.phar /usr/local/bin/composer",
    require => Package["php5-cli"]
  }


}