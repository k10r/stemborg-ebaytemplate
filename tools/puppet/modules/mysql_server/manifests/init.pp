class mysql_server {

    package { 'mysql-server':
        ensure => present
    }

}