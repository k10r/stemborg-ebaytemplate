class apache2 {
  package { "apache2":
      ensure => present,
    }

  file { '/var/lock/apache2':
     ensure => 'absent',
      force => true,
      require => File['/var/www']
  }

  file { '/etc/apache2/envvars':
         ensure => '/vagrant/tools/puppet/files/apache2/envvars',
          force => true,
          require => File['/var/lock/apache2']
  }


  file { '/var/www':
        ensure => '/vagrant',
        force => true,
        require => Exec["enable apache2-modules"]
    }


  exec { 'enable apache2-modules':
          command => '/usr/bin/sudo /usr/sbin/a2enmod rewrite && /usr/bin/sudo /usr/sbin/a2enmod php5',
          require => Package["apache2"]
  }

  file { '/etc/apache2/sites-available/default':
      owner  => root,
      group  => root,
      ensure => file,
      mode   => 644,
      source => '/vagrant/tools/puppet/files/apache2/default',
      require => Package["apache2"]
  }



  exec { "restart apache":
      command => "/usr/bin/sudo /etc/init.d/apache2 restart",
      require => File['/etc/apache2/sites-available/default']
    }
}
