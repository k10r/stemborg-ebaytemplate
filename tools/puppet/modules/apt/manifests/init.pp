class apt {

  # Ensure apt-get update has been run before installing any packages
  Exec["apt-update"] -> Package <| |>

  exec { "apt-update":
      command => "/usr/bin/apt-get update"
  }
}