$extlookup_datadir = "local"
$extlookup_precedence = ["data"]

node 'lucid32' {
    include apt
    include php
    include apache2
    include curl
    include git
    include mysql_client
    include mysql_server
    include vim
    include memcached
    include sendmail
    include phpmyadmin
}