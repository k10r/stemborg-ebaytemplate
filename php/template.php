<?php

// Hier wird die Lagernummer an die DB übergeben und die dazugehörigen Daten ausgelesen

function get_product($id) {
    $db = pg_connect("host=localhost port=5432 dbname=cs_minimalist user=dominik password=AUGEN1601loch") or die("could not connect: " . pg_last_error());
    
    $product = array(
        'text' => array(),
        'stammid' => array(),
        'crossSelling' => array(),
        'images' => array()
    );

    $article_query = 'SELECT ebay FROM article WHERE id = ' . pg_escape_string($id);
    $article_result = pg_query($article_query) or die("could not request article for $id: " . pg_last_error());
    $article = pg_fetch_all($article_result);
    $product['ebay'] = $article[0]['ebay'];

    $text_query = 'SELECT content FROM article_text WHERE id = ' . pg_escape_string($id) . ' ORDER BY priority ASC';
    $text_result = pg_query($text_query) or die("could not request article_text for $id: " . pg_last_error());
    while ($line = pg_fetch_array($text_result, null, PGSQL_ASSOC)) {
        $product['text'][] = $line['content'];
    }

// alle stammids die zur Lagernummer gehören (für SOAP-Aufruf) zum Updaten den Artikel direkt in DR
    $stamm_id_query = 'SELECT stammid FROM article_stammid WHERE id = ' . pg_escape_string($id);
    $stamm_id_result = pg_query($stamm_id_query) or die("could not request article_stammid for $id: " . pg_last_error());
    while ($line = pg_fetch_array($stamm_id_result, null, PGSQL_ASSOC)) {
        $product['stammid'][] = $line['stammid'];
    }

// Hier beginnt CS
    $cs_query = 'SELECT cs_id FROM article_article WHERE id = ' . pg_escape_string($id);
    $cs_result = pg_query($cs_query) or die("could not request article_article for $id: " . pg_last_error());
    while ($line = pg_fetch_array($cs_result, null, PGSQL_ASSOC)) {
        $cs_article_query = 'SELECT ebay FROM article WHERE id = ' . pg_escape_string($line['cs_id']);
        $cs_article_result = pg_query($cs_article_query) or die("could not request article for " . $line['cs_id'] . ": " . pg_last_error());
        $cs_article = pg_fetch_all($cs_article_result);
        // just to ensure that static template generation has values for price+title
        $cs_article[0]['price'] = 0;
        $cs_article[0]['title'] = '';
        $product['crossSelling'][] = $cs_article[0];
    }
    
    $images_query = 'SELECT image FROM article_image WHERE id = ' . pg_escape_string($id) . ' ORDER BY priority ASC';
    $images_result = pg_query($images_query) or die("could not request article_image for $id: " .pg_last_error());
    while ($line = pg_fetch_array($images_result, null, PGSQL_ASSOC)) {
        $product['images'][] = $line['image'];
    }
    
    return $product;
}

function get_template($product) {
    $p = array_merge(
        $product,
        array(
            'nav' => array(
                array(
                    'storeId' => 'Stemborg_Drucker-Scanner-Zubehor_W0QQfsubZ6430067016',
                    'title' => 'Drucker, Scanner &amp; Zubeh&ouml;r'
                ),
                array(
                    'storeId' => 'Stemborg_Haushaltsgerate_W0QQfsubZ4990330016',
                    'title' => 'Haushaltsgeräte'
                ),
                array(
                    'storeId' => 'Stemborg_Mobel-Wohnen_W0QQfsubZ6430068016',
                    'title' => 'M&ouml;bel &amp; Wohnen'
                ),
                array(
                    'storeId' => 'Stemborg_Spielzeug_W0QQfsubZ6430069016',
                    'title' => 'Spielzeug'
                ),
                array(
                    'storeId' => 'Stemborg_Garten-Terasse_W0QQfsubZ6430070016',
                    'title' => 'Garten &amp; Terasse'
                ),
                array(
                    'storeId' => 'Stemborg_Camping-Outdoor_W0QQfsubZ6430071016',
                    'title' => 'Camping &amp; Outdoor'
                ),
                array(
                    'storeId' => 'Stemborg_Haustierbedarf_W0QQfsubZ6430072016',
                    'title' => 'Haustierbedarf'
                )
            ),
            'contact' => array(
                'name' => 'Giovanni Rattu',
                'tel' => '08001316190',
                'fax' => '0620529299924',
                'email' => 'ebay@shoppingprojekt.de'
            ),
            'social' => array(
                array(
                    'url' => 'https://www.facebook.com/stemborg',
                    'title' => 'facebook.com/stemborg',
                    'faIcon' => 'fa-facebook-square',
                ),
                array(
                    'url' => 'skype:stemborg_kundenservice?call',
                    'title' => 'stemborg_kundenservice',
                    'faIcon' => 'fa-skype'
                ),
                array(
                    'url' => 'https://plus.google.com/109951847497616360735',
                    'title' => 'google.com/+109951847497616360735',
                    'faIcon' => 'fa-google-plus-square'
                )
            )
        )
    );
    
    ob_start();
    include 'tpl/product.tpl.php';
    return ob_get_clean();
}

function handle_non_success($soapResult) {
    if ($soapResult->ack != 'success') {
        error_log($soapResult->ack . ': ' . implode(',', $soapResult->errors));
    }
}

function update_ebay_description($client, $auth, $description, $stamm_id) {
    $request = array();
    $item = array();

    $item['stamm'] = array(
        'stamm_id' => $stamm_id
    );
    $item['ebay'] = array(
        'stamm_id' => $stamm_id,
        'artikelbeschreibung_zlib' => base64_encode($description),
        'vorlagen_id' => 0
    );
    $request['listerlist'][] = $item;

    $result = $client->__soapCall('set_lister', array($auth, $request));
    handle_non_success($result);

    return $result->ack;
}

function trigger_ebay_update($client, $auth, $stamm_id) {
    $result = $client->__soapCall('set_ebay_update', array($auth, array(
        'stamm_id' => $stamm_id,
        'update_fields' => array('desc')
    )));
    handle_non_success($result);
    
    return $result->ack;
}

function update_dreamrobot($id) {
    $product = get_product($id);
    $description = get_template($product);
    
    ini_set('soap.wsdl_cache_enabled', '0');
    $client = new soapclient('http://www.dreamrobot.de/soap/drsoapinterface_v_1.2.php?wsdl', array('trace'=>1));
    $auth = array(
        'user_id' => '5160',
        'user' => 'shoppingprojekt5160eb',
        'pw' => 'nbgYw'
    );
    
    try {
        $results = array();
        foreach ($product['stammid'] as $stammid) {
            $status_update = update_ebay_description($client, $auth, $description, $product['stammid'][0]);
            $status_trigger = trigger_ebay_update($client, $auth, $product['stammid'][0]);
            
            $results[$stammid] = array(
                'descriptionUpdate' => $status_update,
                'ebayUpdateTriggered' => $status_trigger
            );
        }
        header('Content-Type: application/json');
        echo json_encode($results);
    } catch (Exception $e) {
        error_log($e->getMessage());
        error_log($e->getTraceAsString());
    }
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $method = $_SERVER['REQUEST_METHOD'];
    
    if ($method == 'GET') {
        if (isset($_GET['callback'])) {
            header('Content-Type: application/javascript');
            echo $_GET['callback'] . '(' . json_encode(get_product($id)) . ');';
        } else {
            header('Content-Type: text/html');
            echo get_template(get_product($id));
        }
    } else if ($method == 'POST') {
        update_dreamrobot($id);
    }
}
?>