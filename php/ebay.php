<?php
libxml_disable_entity_loader(false);

$strAppid = 'Stemborg-0734-4279-a3fa-76f8b954470d';
$intSiteId = 77;

/*
 * echos $json as jsonp if ?callback= is set,
 * as json otherwise
 */
function jsonp($json) {
    if (isset($_GET['callback'])) {
        header('Content-Type: application/javascript');
        echo "{$_GET['callback']}($json);";
    } else {
        header('Content-Type: application/json');
        echo $json;
    }
}

/*
 * calls the ebay api to get information
 * for a single ebay item.
 *
 * returns it as xml
 */
function getSingleItem($intId) {
    global $strAppid, $intSiteId;
    
    $strGetSingleItemApiUrl = 'http://open.api.ebay.com/shopping?'
        . 'callname=GetSingleItem&'
        . 'responseencoding=XML&'
        . 'appid=' . $strAppid . '&'
        . 'siteid=' . $intSiteId . '&'
        . 'version=515&'
        . 'ItemID=' . $intId;

    return simplexml_load_file($strGetSingleItemApiUrl);
}

if (isset($_GET['id']) && preg_match('/^\d+$/i', $_GET['id'])) {
    $intId = $_GET['id'];
    if (isset($_GET['endpoint'])) {
        $endpoint = $_GET['endpoint'];
        
        if ($endpoint == 'GetSingleItem') {
            jsonp(json_encode(getSingleItem($intId)));
        }
    }
}
?>